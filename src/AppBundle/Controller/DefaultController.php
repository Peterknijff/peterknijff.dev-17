<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AppBundle::index.html.twig', array());
    }

    public function aboutAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aboutme = $em->getRepository('AppBundle:AboutMe')->findOneBy([]);
        $experiences = $em->getRepository('AppBundle:Experience')->findBy(['enabled' => 1], ['dateEnd' => 'DESC']);


        return $this->render('@App/about_me.html.twig', array(
            'aboutme' => $aboutme,
            'experiences' => $experiences,
        ));
    }


    public function contactAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $aboutme = $em->getRepository('AppBundle:AboutMe')->findOneBy([]);

        // Create the form
        $form = $this->createForm('AppBundle\Form\Type\ContactType',null,array(
            'action' => $this->generateUrl('contact'),
            'method' => 'POST'
        ));

        if ($request->isMethod('POST')) {
            // Refill the fields in case the form is not valid.
            $form->handleRequest($request);

            if($form->isValid()){
                // Send mail
                if($this->sendEmail($form->getData())){

                    // Everything OK, redirect to wherever you want ! :

                    return $this->redirectToRoute('contact');
                }else{
                    // An error ocurred, handle
                    var_dump("Error");
                }
            }
        }

        return $this->render('@App/contact.html.twig', array(
            'aboutme' => $aboutme,
            'form' => $form->createView()
        ));
    }

    private function sendEmail($data){
        $myappContactMail = $this->container->getParameter('mailer_user');;
        $myappContactPassword = 'yourmailpassword';

        // In this case we'll use the ZOHO mail services.
        // If your service is another, then read the following article to know which smpt code to use and which port
        // http://ourcodeworld.com/articles/read/14/swiftmailer-send-mails-from-php-easily-and-effortlessly
        $transport = \Swift_SmtpTransport::newInstance('smtp.zoho.com', 465,'ssl')
            ->setUsername($myappContactMail)
            ->setPassword($myappContactPassword);

        $mailer = \Swift_Mailer::newInstance($transport);

        $message = \Swift_Message::newInstance("Our Code World Contact Form ". $data["subject"])
            ->setFrom(array($myappContactMail => "Message by ".$data["name"]))
            ->setTo(array(
                $myappContactMail => $myappContactMail
            ))
            ->setBody($data["message"]."<br>ContactMail :".$data["email"]);

        return $mailer->send($message);
    }
}
