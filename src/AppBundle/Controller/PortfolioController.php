<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PortfolioController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $projectRepo = $em->getRepository('AppBundle:Project');
        $categoryRepo = $em->getRepository('AppBundle:Category');
        $projects = $projectRepo->getActiveProjects();
        $categories = array();
        $projectsArray = array();
        $projectCategoryCount = array();

        // Loop over projects
        foreach($projects as $project){
            // Loop over categories in each project
            foreach($project->getCategories() as $category){
                // Count howmany projects there are in category
                $category = $categoryRepo->getAvailableProjects($category)[0];
                if(isset($projectCategoryCount[$category->getTitle()])){
                    $projectCategoryCount[$category->getTitle()]['count'] = $projectCategoryCount[$category->getTitle()]['count']+1;
                }else{
                    $projectCategoryCount[$category->getTitle()] = [
                        'slug' => $category->getSlug(),
                        'count' => 1
                    ];
                }
                if($category->isEnabled() || !empty($categories)){
                $projectsArray[] = $project;
                    $categories[] = $category;
                }
            }
        }
        $projectsArray = array_unique($projectsArray);
        $categories = array_unique($categories);
        return $this->render('AppBundle::portfolio.html.twig', array(
            'projects' => $projectsArray,
            'categories' => $categories,
            'categoriesCount' => $projectCategoryCount
        ));
    }

    public function projectAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $projectRepo = $em->getRepository('AppBundle:Project');
        $project = $projectRepo->findOneBy(['slug' => $slug]);

        if(empty($project)){
            return $this->redirectToRoute('index', array(), 301);
        }

        $projects = $projectRepo->findBy(['enabled' => 1], ['datetime' => 'DESC']);
        $index = array_search($project, $projects);

        if($index != 0 && $index != sizeof($projects)-1){
            $previous = $projects[$index - 1];
            $next = $projects[$index + 1];
        }else{
            if($index == 0){
                $previous = $projects[sizeof($projects) - 1];
                $next = $projects[$index + 1];
            }else{
                $previous = $projects[$index - 1];
                $next = $projects[0];
            }
        }

        return $this->render('AppBundle:portfolio:project.html.twig', array(
            'project' => $project,
            'previous' => $previous,
            'next' => $next
        ));
    }
}
