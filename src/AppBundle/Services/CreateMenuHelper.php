<?php
namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;

class CreateMenuHelper  {

    private $menu;
    private $factory;
    private $em;

    function __construct(FactoryInterface $factory, EntityManager $em)
    {
        $this->factory = $factory;
        $this->em = $em;
    }

    public function generateMenu($rootAttributes, $menuItems){

        $this->menu = $this->factory->createItem('root');

        if(isset($rootAttributes['class']))
        {
            $this->menu->setChildrenAttribute('class', $rootAttributes['class']);
        }else if(isset($rootAttributes['id']))
        {
            $this->menu->setChildrenAttribute('id', $rootAttributes['id']);
        }

        foreach ($menuItems as $menuItem) {

            $menuItemName = key( array_slice( $menuItem, 0, 1, true ));
            isset($menuItem['class']) ? $class = $menuItem['class'] : $class = "";

            if($menuItem[$menuItemName] == 'button'){
                if(isset($menuItem['route'])){
                    $this->menu->addChild($menuItemName, array('route' => $menuItem['route']))
                        ->setAttribute('class', 'nav-item '.$class  );
                }else if(isset($menuItem['uri'])){
                    $this->menu->addChild($menuItemName, array('uri' => $menuItem['uri']))
                        ->setAttribute('class', 'nav-item '.$class  )
                        ->setAttribute('class-a', 'nav-item '.$menuItem['class-a']  )
                        ->setAttribute('icon', 'fa fa-'.$menuItem['icon'])
                        ->setAttribute('target', $menuItem['target']  );


                }
            }else if($menuItem[$menuItemName] == 'tab'){
                if(isset($menuItem['icon'])){
                    if($menuItem['icon'] == "Settings"){
                        if(isset($menuItem['attributes']['route'])){
                            $this->menu->addChild($menuItemName, array('route' => $menuItem['attributes']['route']))
                                ->setAttribute('class', 'page_settings')
                                ->setAttribute('hideLabel', true)
                                ->setAttribute('icon', 'fa fa-'.$menuItem['icon']);
                        }
                    }else{
                        if(isset($menuItem['attributes']['route'])){
                            $this->menu->addChild($menuItemName, array('route' => $menuItem['attributes']['route']))
                                ->setAttribute('icon', 'fa fa-'.$menuItem['icon']);
                        }
                    }
                }else{
                    if(isset($menuItem['attributes']['route'])){
                        $this->menu->addChild($menuItemName, array('route' => $menuItem['attributes']['route']));
                    }
                }
            }elseif($menuItem[$menuItemName] == 'static'){
                $this->menu->addChild($menuItemName, array('route' => $menuItem['attributes']['route']))
                    ->setAttribute('hideLabel', true)
                    ->setAttribute('icon', 'fa fa-'.$menuItem['icon']);
            }elseif($menuItem[$menuItemName] == 'dropdown'){

                isset($menuItem['class']) ? $class = $menuItem['class'] : $class = "";

                $this->menu->addChild($menuItemName, array('uri' => "#" ) )
                    ->setAttribute('hideLabel', $menuItem['hideLabel'])
                    ->setAttribute('dropdown', true)
                    ->setAttribute('class', 'nav-item dropdown '.$class  )
                    ->setAttribute('icon', 'fa fa-'.$menuItem['icon']);

                //Submenu items of dropdown
                $attributeArray = [];
                $updateCount = 0;
                $i = 0;
                $y = 1;

                foreach($menuItem['attributes'] as $attributes){
                    $attributeArray[] = key( array_slice($menuItem['attributes'], $i, $y++, true ));
                    $updateCount = $this->addAttributes($this->menu, $menuItemName, $attributeArray[$i], $attributes, $updateCount);
                    $i++;
                }

                //Are there any updates?
                if($updateCount > 0){
                    $this->menu[$menuItemName]->setAttribute('update', $updateCount);
                }
            }
            // Set update value's
            if(isset($menuItem['attributes']['update'])){
                $this->menu[$menuItemName]->setAttribute('update', $menuItem['attributes']['update']);
            }
        }
        return $this->menu;
    }

    public function addAttributes(ItemInterface $menu, $menuItemName, $subMenuItemName, $attributes, $updateCount)
    {
        $menu[$menuItemName]->addChild($subMenuItemName, array('route' => $attributes['route']));

        if(isset($attributes['divider_prepend'])){
            $menu[$menuItemName][$subMenuItemName]->setAttribute('divider_prepend', true);
        }

        // Maybe in future, check here for URI value
        if(isset($attributes['icon'])){
            $menu[$menuItemName][$subMenuItemName]->setAttribute('icon', 'fa fa-'.$attributes['icon']);
        }
        if(isset($attributes['update'])){
            $menu[$menuItemName][$subMenuItemName]->setAttribute('update', $attributes['update']);

            // Sums up update values for menu item update value
            $updateCount += $attributes['update'];
        }
        return $updateCount;
    }

}