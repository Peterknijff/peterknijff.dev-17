<?php namespace AppBundle\Menu;

use AppBundle\Services\CreateMenuHelper;

class MainMenuBuilder {

    private $menuHelper;
    private $container;

    function __construct(CreateMenuHelper $menuHelper)
    {
        $this->menuHelper = $menuHelper;
    }

    public function createMainMenu(array $options)
    {
//        $aboutMe = $this->container->get('doctrine.orm.default_entity_manager')->getRepository('AppBundle:AboutMe')->findOneBy(['id' => 1]);
//        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
//        $path = $helper->asset($aboutMe, 'documentFile');

        // Menu items, each wrapped in an array for generation process
        $menuItems = array(
            array('Over mij' => 'button',
                'route' => 'about',
                'target' => '_self',
            ),
            array('Portfolio' => 'button',
                'route' => 'portfolio',
                'target' => '_self',
            ),
            array('Contact' => 'button',
                'route' => 'contact',
                'target' => '_self',
            ),
            array('Download CV' => 'button',
                'class' => 'nav-item-cta ',
                'class-a' => 'btn btn-cta btn-cta-secondary',
                'target' => '_blank',
                'icon' => 'download',
                'uri' => '/uploads/doc/593a881aab3b9_CV-Peter-Knijff-2017.pdf',
            ),
        );
        return $this->menuHelper->generateMenu(array('class' => 'nav navbar-nav'), $menuItems);
    }

}