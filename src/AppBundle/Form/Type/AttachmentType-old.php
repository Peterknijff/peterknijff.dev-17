<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\CategoryBundle\Entity\Category;
use App\ProductBundle\Entity\ProductSpecification;

class CategoryHasSpecsType extends AbstractType {

    private $category;
    private $specs = array();

    public function __construct(Category $category) {
        $this->category = $category;

//        echo '<pre>';
//        \Doctrine\Common\Util\Debug::dump($category->getSpecs());
//        echo '</pre>';
//die();

        foreach($category->getSpecs() as $specification) {
            $this->specs[] = $specification->getSpecification();
        }
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('specification', 'entity', array(
                'empty_value' => 'Selecteer een specificatie',
                'class' => 'ProductBundle:ProductSpecification',
                'label' => 'Specificatie:',
            )
        )
            ->add('position', 'integer', array(
                    'label' => 'Positie:'
                )
            )
            ->add('category', 'entity_hidden', array(
                    'class' => 'CategoryBundle:Category',
                    'label' => false,
                    'data_class' => null,
                    'data' => $this->category,
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'App\CategoryBundle\Entity\CategoryHasSpecification',
            )
        );
    }

    public function getName()
    {
        return 'category_has_specs';
    }

}