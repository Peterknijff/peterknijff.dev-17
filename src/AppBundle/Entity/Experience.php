<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Project;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\Traits as Traits;

/**
 * Experience
 *
 * @ORM\Table(name="experience")
 * @ORM\Entity()
 * @Vich\Uploadable
 */
class Experience
{
    use Traits\Identifiable,
        Traits\TitleAble,
        Traits\SubtitleAble,
        Traits\TimeStampable,
        Traits\Enableable;

    /**
     * @var Client
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="experiences")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=true)
     **/
    protected $client;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=true)
     **/
    protected $project;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="datetime")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="datetime")
     */
    private $dateEnd;

    /**
     * Set the Client
     *
     * @param Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * Get Client.
     *
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set the Project
     *
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * Get Project.
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return $this
     */
    public function setDateStart($date)
    {
        $this->dateStart = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }
    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return $this
     */
    public function setDateEnd($date)
    {
        $this->dateEnd = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }


}

