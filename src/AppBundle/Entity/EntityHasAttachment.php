<?php

namespace AppBundle\Entity;

use AppBundle\Repository\AttachmentRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * EntityHasAttachment
 *
 * @ORM\Table(name="project_attachments")
 * @ORM\Entity
 */
class EntityHasAttachment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\ManyToOne(targetEntity="Project", inversedBy="attachments") */
    protected $project;

    /** @ORM\ManyToOne(targetEntity="Attachment") */

    protected $attachment;

    //TODO intern class with types
    /**
     * @ORM\Column(name="type", type="string")
     */
    private $type;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->attachment->getTitle();
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return EntityHasAttachment
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set project
     *
     * @param Project $project
     * @return EntityHasAttachment
     */
    public function setProject(Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set attachment
     *
     * @param Attachment $attachment
     * @return EntityHasAttachment
     */
    public function setAttachment(Attachment $attachment = null)
    {
        $this->attachment = $attachment;

        return $this;
    }

    /**
     * Get specification
     *
     * @return EntityHasAttachment
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

}
