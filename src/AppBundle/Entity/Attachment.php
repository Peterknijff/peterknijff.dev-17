<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Traits;
// GEDMO IS USED HERE BY TRAITS
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Attachment
 *
 * @ORM\Table(name="attachment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttachmentRepository")
 * @Vich\Uploadable
 */
class Attachment
{

    use Traits\Identifiable,
        Traits\TitleAble,
        Traits\AttachAble,
        Traits\TimeStampable;
}
