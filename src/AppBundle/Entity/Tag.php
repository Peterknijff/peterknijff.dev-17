<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Traits as Traits;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Tags
 *
 * @ORM\Table(name="tag")
 * @ORM\Entity
 */
class Tag
{
    use Traits\Identifiable,
        Traits\TitleAble,
        Traits\Sluggable,
        Traits\SubtitleAble,
        Traits\TimeStampable;

}

