<?php

namespace AppBundle\Entity;

use AppBundle\Entity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
// GEDMO IS USED HERE BY TRAITS
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\Traits as Traits;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 * @Vich\Uploadable
 */
class Category
{
    use Traits\Identifiable,
        Traits\TitleAble,
        Traits\SubtitleAble,
        Traits\ImageAble,
        Traits\TimeStampable,
        Traits\Enableable,
        Traits\Sluggable;

    /**
     * Constructor
     *
     * ArrayCollection
     */
    public function __construct() {
        $this->projects = new ArrayCollection();
    }

    /**
     * This unmapped property stores the binary contents of the image file
     * associated with the entity.
     *
     * @Vich\UploadableField(mapping="category_image", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    /**
     * Project in the category.
     *
     * @var Project[]
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Project", mappedBy="categories")
     **/
    protected $projects;

    /**
     * The category parent.
     *
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     **/
    protected $parent;


    /**
     * Set the parent category.
     *
     * @param Category $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * Get the parent category.
     *
     * @return Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Return all portfolio associated to the category.
     *
     * @return Project[]
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * Set all portfolio in the category.
     *
     * @param Project[] $projects
     */
    public function setProjects($projects)
    {
        $this->projects->clear();
        $this->projects = new ArrayCollection($projects);
    }

    /**
     * Add a project in the category.
     *
     * @param $project project The project to associate
     */
    public function addProject($project)
    {
        if ($this->projects->contains($project)) {
            return;
        }

        $this->projects->add($project);
        $project->addCategory($this);
    }

    /**
     * @param project $project
     */
    public function removeProject($project)
    {
        if (!$this->projects->contains($project)) {
            return;
        }

        $this->projects->removeElement($project);
        $project->removeCategory($this);
    }
}

