<?php

namespace AppBundle\Entity;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\Traits as Traits;

/**
 * AboutMe
 *
 * @ORM\Table(name="about_me")
 * @ORM\Entity()
 * @Vich\Uploadable
 */
class AboutMe
{
    use Traits\Identifiable,
        Traits\TitleAble,
        Traits\SubtitleAble,
        Traits\Describable,
        Traits\TimeStampable,
        Traits\ImageAble;


    /**
     * This unmapped property stores the binary contents of the image file
     * associated with the entity.
     *
     * @Vich\UploadableField(mapping="profile_image", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    //Slogan
    /**
     * @var string
     *
     * @ORM\Column(name="slogan", type="string", length=255)
     */
    private $slogan;

    /**
     * Set slogan
     *
     * @param string $slogan
     *
     * @return $this
     */
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;

        return $this;
    }

    /**
     * Get slogan
     *
     * @return string
     */
    public function getSlogan()
    {
        return $this->slogan;
    }


    /**
     * It only stores the name of the thumbnail associated with the entity.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $document;

    /**
     * This unmapped property stores the binary contents of the document file
     * associated with the entity.
     *
     * @Vich\UploadableField(mapping="cv_file", fileNameProperty="document")
     *
     * @var File
     */
    private $documentFile;

    /**
     * @param File $document
     */
    public function setDocumentFile(File $document = null)
    {
        $this->documentFile = $document;
        if ($document instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return File
     */
    public function getDocumentFile()
    {
        return $this->documentFile;
    }

    /**
     * @param string $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

    /**
     * @return string
     */
    public function getDocument()
    {
        return $this->document;
    }

    //EMAIL
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * Set email
     *
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    //LINKEDIN
    /**
     * @var string
     *
     * @ORM\Column(name="linked_in", type="string", length=255)
     */
    private $linkedIn;

    /**
     * Set linkedIn
     *
     * @param string $linkedIn
     *
     * @return $this
     */
    public function setLinkedIn($linkedIn)
    {
        $this->linkedIn = $linkedIn;

        return $this;
    }

    /**
     * Get linkedIn
     *
     * @return string
     */
    public function getLinkedIn()
    {
        return $this->linkedIn;
    }

    //INSTAGRAM
    /**
     * @var string
     *
     * @ORM\Column(name="instagram", type="string", length=255)
     */
    private $instagram;

    /**
     * Set instagram
     *
     * @param string $instagram
     *
     * @return $this
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;

        return $this;
    }

    /**
     * Get instagram
     *
     * @return string
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    //PHONE
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    //ADDRESS
    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text")
     */
    private $address;

    /**
     * Set address
     *
     * @param string $address
     *
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

}

