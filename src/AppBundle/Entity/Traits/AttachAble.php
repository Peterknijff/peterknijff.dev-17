<?php

namespace AppBundle\Entity\Traits;

use Symfony\Component\HttpFoundation\File\File;

trait AttachAble
{
    /**
     * It only stores the name of the attachment associated with the project.
     *
     * @ORM\Column(name="file_name", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $attachment;

    /**
     * This unmapped property stores the binary contents of the image attachment
     * associated with the project.
     *
     * @Vich\UploadableField(mapping="project_attachment", attachmentNameProperty="attachment")
     *
     * @var File
     */
    private $attachmentFile;

    /**
     * @param File $file
     */
    public function setAttachmentFile(File $file = null)
    {
        $this->attachmentFile = $file;

        if ($file instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return File
     */
    public function getAttachmentFile()
    {
        return $this->attachmentFile;
    }

    /**
     * @param string $file
     */
    public function setAttachment($file)
    {
        $this->attachment = $file;
    }

    /**
     * @return string
     */
    public function getAttachment()
    {
        return $this->attachment;
    }
}