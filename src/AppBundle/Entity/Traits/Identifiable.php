<?php

namespace AppBundle\Entity\Traits;


trait Identifiable {

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type = "integer")
     * @ORM\GeneratedValue(strategy = "IDENTITY")
     */
    protected $id;
    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

}