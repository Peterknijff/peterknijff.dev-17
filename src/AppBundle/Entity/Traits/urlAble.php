<?php

namespace AppBundle\Entity\Traits;

trait urlAble
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="url", type="string", nullable=true)
     *
     */
    private $url;

    /**
     * Set url
     *
     * @param string $url
     *
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
}