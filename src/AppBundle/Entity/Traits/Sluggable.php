<?php

namespace AppBundle\Entity\Traits;

trait Sluggable {

    /**
     * @var string
     *
     * @ORM\Column(name="slug", length=32, unique=true)
     * @Gedmo\Slug(fields={"title"}, updatable=true)
     */
    protected $slug;

    /**
     * get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

}