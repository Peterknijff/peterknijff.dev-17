<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Traits as Traits;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;


/**
 * Techniques
 *
 * @ORM\Table(name="technique")
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Technique
{

    use Traits\Identifiable,
        Traits\TitleAble,
        Traits\Sluggable,
        Traits\ImageAble,
        Traits\SubtitleAble,
        Traits\TimeStampable;

    public function __construct() {
        $this->projects = new ArrayCollection();
    }

    /**
     * This unmapped property stores the binary contents of the image file
     * associated with the entity.
     *
     * @Vich\UploadableField(mapping="technique_image", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Project", mappedBy="techniques")
     **/
    private $projects;

    /**
     * @return mixed
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * @param mixed $projects
     */
    public function setProjects($projects)
    {
        $this->projects = $projects;
    }


}

