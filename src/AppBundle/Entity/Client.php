<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Traits as Traits;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Client
{
    use Traits\Identifiable,
        Traits\TitleAble,
        Traits\Sluggable,
        Traits\urlAble,
        Traits\ImageAble,
        Traits\SubtitleAble,
        Traits\TimeStampable,
        Traits\Enableable;

    public function __construct() {
        $this->projects = new ArrayCollection();
        $this->experiences = new ArrayCollection();
    }

    /**
     * This unmapped property stores the binary contents of the image file
     * associated with the entity.
     *
     * @Vich\UploadableField(mapping="client_logo", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Project", mappedBy="client")
     **/
    private $projects;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Experience", mappedBy="client")
     **/
    private $experiences;

    /**
     * @return mixed
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * @param mixed $project
     */
    public function setProjects($project)
    {
        $this->projects = $project;
    }

    /**
     * Set the Experiences
     *
     * @param ArrayCollection $experiences
     */
    public function setExperiences($experiences)
    {
        $this->experiences = $experiences;
    }

    /**
     * Get experiences.
     *
     * @return ArrayCollection
     */
    public function getExperiences()
    {
        return $this->experiences;
    }

}

