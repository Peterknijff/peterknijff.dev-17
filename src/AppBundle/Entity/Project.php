<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Category;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use AppBundle\Entity\Traits as Traits;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectRepository")
 * @Vich\Uploadable
 */
class Project
{
    // Non-referencing Traits
    use Traits\Identifiable,
        Traits\TitleAble,
        Traits\SubtitleAble,
        Traits\Sluggable,
        Traits\urlAble,
        Traits\cssAble,
        Traits\DateTimeAble,
        Traits\TimeStampable,
        Traits\ArchiveAble,
        Traits\Enableable;

    public function __construct() {
        $this->categories = new ArrayCollection();
        $this->techniques = new ArrayCollection();
        $this->tags = new ArrayCollection();
//        $this->attachments = new ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="intro", type="text",  nullable=true)
     */
    private $intro;

    /**
     * @var string
     *
     * @ORM\Column(name="challenge", type="text",  nullable=true)
     */
    private $challenge;


    /**
     * @var string
     *
     * @ORM\Column(name="approach", type="text",  nullable=true)
     */
    private $approach;

    /**
     * @var string
     *
     * @ORM\Column(name="result", type="text",  nullable=true)
     */
    private $result;

    /**
     * List of categories where the portfolio is
     * (Owning side).
     *
     * @var Category[]
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Category", inversedBy="projects")
     * @ORM\JoinTable(name="project_category")
     */
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Technique", inversedBy="projects")
     *
     * @ORM\JoinTable(name="project_techniques")
     **/
    private $techniques;

    /**
     * Many Projects have Many Tags.
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Tag")
     * @ORM\JoinTable(name="project_tags",
     *      joinColumns={@ORM\JoinColumn(name="project_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     *      )
     */
    private $tags;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client", inversedBy="projects")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     **/
    private $client;


//    /**
//     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Attachment", mappedBy="" cascade={"persist", "remove"})
//     */
//    private $attachments;

    /**
     * It only stores the name of the thumbnail associated with the project.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $thumb;

    /**
     * This unmapped property stores the binary contents of the image file
     * associated with the project.
     *
     * @Vich\UploadableField(mapping="project_thumb", fileNameProperty="thumb")
     *
     * @var File
     */
    private $thumbFile;

    /**
     * It only stores the name of the slider image associated with the project.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $slider;

    /**
     * This unmapped property stores the binary contents of the image file
     * associated with the project.
     *
     * @Vich\UploadableField(mapping="project_slider", fileNameProperty="slider")
     *
     * @var File
     */
    private $sliderFile;

    /**
     * It only stores the name of the slider image associated with the project.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $sloganBg;

    /**
     * This unmapped property stores the binary contents of the image file
     * associated with the project.
     *
     * @Vich\UploadableField(mapping="project_slogan_bg", fileNameProperty="sloganBg")
     *
     * @var File
     */
    private $sloganBgFile;

    /**
     * @var string
     *
     * @ORM\Column(name="slogan", type="string", length=255, nullable=true)
     */
    private $slogan;

    // APPROACH IMAGE
    /**
     * It only stores the name of the approach image associated with the project.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $approachImage;

    /**
     * This unmapped property stores the binary contents of the image file
     * associated with the project.
     *
     * @Vich\UploadableField(mapping="project_approach", fileNameProperty="approachImage")
     *
     * @var File
     */
    private $approachFile;

    /**
     * @var string
     *
     * @ORM\Column(name="approach_caption", type="string", length=255)
     */
    private $approachCaption;

    // APPROACH IMAGE 1
    /**
     * It only stores the name of the approach image associated with the project.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $approachImage1;

    /**
     * This unmapped property stores the binary contents of the image file
     * associated with the project.
     *
     * @Vich\UploadableField(mapping="project_approach1", fileNameProperty="approachImage1")
     *
     * @var File
     */
    private $approachFile1;

    /**
     * @var string
     *
     * @ORM\Column(name="approach_caption1", type="string", length=255)
     */
    private $approachCaption1;

    // APPROACH IMAGE 2
    /**
     * It only stores the name of the approach image associated with the project.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $approachImage2;

    /**
     * This unmapped property stores the binary contents of the image file
     * associated with the project.
     *
     * @Vich\UploadableField(mapping="project_approach2", fileNameProperty="approachImage2")
     *
     * @var File
     */
    private $approachFile2;

    /**
     * @var string
     *
     * @ORM\Column(name="approach_caption2", type="string", length=255)
     */
    private $approachCaption2;

    // RESULT IMAGE
    /**
     * It only stores the name of the approach image associated with the project.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $resultImage;

    /**
     * This unmapped property stores the binary contents of the image file
     * associated with the project.
     *
     * @Vich\UploadableField(mapping="project_result", fileNameProperty="resultImage")
     *
     * @var File
     */
    private $resultFile;

    /**
     * @var string
     *
     * @ORM\Column(name="result_caption", type="string", length=255)
     */
    private $resultCaption;

    // RESULT IMAGE1
    /**
     * It only stores the name of the approach image associated with the project.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $resultImage1;

    /**
     * This unmapped property stores the binary contents of the image file
     * associated with the project.
     *
     * @Vich\UploadableField(mapping="project_result1", fileNameProperty="resultImage1")
     *
     * @var File
     */
    private $resultFile1;

    /**
     * @var string
     *
     * @ORM\Column(name="result_caption1", type="string", length=255)
     */
    private $resultCaption1;

    // RESULT IMAGE2
    /**
     * It only stores the name of the approach image associated with the project.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $resultImage2;

    /**
     * This unmapped property stores the binary contents of the image file
     * associated with the project.
     *
     * @Vich\UploadableField(mapping="project_result2", fileNameProperty="resultImage2")
     *
     * @var File
     */
    private $resultFile2;

    /**
     * @var string
     *
     * @ORM\Column(name="result_caption2", type="string", length=255)
     */
    private $resultCaption2;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="text")
     */
    private $subtitle;

    /**
     * Set intro
     *
     * @param string $intro
     *
     * @return $this
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;

        return $this;
    }

    /**
     * Get intro
     *
     * @return string
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * Set challenge
     *
     * @param string $challenge
     *
     * @return $this
     */
    public function setChallenge($challenge)
    {
        $this->challenge = $challenge;

        return $this;
    }

    /**
     * Get challenge
     *
     * @return string
     */
    public function getChallenge()
    {
        return $this->challenge;
    }

    /**
     * Set approach
     *
     * @param string $approach
     *
     * @return $this
     */
    public function setApproach($approach)
    {
        $this->approach = $approach;

        return $this;
    }

    /**
     * Get approach
     *
     * @return string
     */
    public function getApproach()
    {
        return $this->approach;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return $this
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Get all associated categories.
     *
     * @return Category[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set all categories of the product.
     *
     * @param Category[] $categories
     */
    public function setCategories($categories)
    {
        // This is the owning side, we have to call remove and add to have change in the category side too.
        foreach ($this->getCategories() as $category) {
            $this->removeCategory($category);
        }
        foreach ($categories as $category) {
            $this->addCategory($category);
        }
    }


    /**
     * Add a category in the product association.
     * (Owning side).
     *
     * @param $category Category the category to associate
     */
    public function addCategory($category)
    {
        if ($this->categories->contains($category)) {
            return;
        }

        $this->categories->add($category);
        $category->addProject($this);
    }

    /**
     * Remove a category in the product association.
     * (Owning side).
     *
     * @param $category Category the category to associate
     */
    public function removeCategory($category)
    {
        if (!$this->categories->contains($category)) {
            return;
        }

        $this->categories->removeElement($category);
        $category->removeProject($this);
    }


    /**
     * Set subtitle
     *
     * @param string $subtitle
     *
     * @return Project
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @return mixed
     */
    public function getTechniques()
    {
        return $this->techniques;
    }

    /**
     * @param mixed $techniques
     */
    public function setTechniques($techniques)
    {
        $this->techniques = $techniques;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }


    /**
     * Set client
     *
     * @param Client $client
     *
     * @return Project
     */
    public function setClient(Client $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }
//
//    /**
//     * Add attachment
//     *
//     * @param \AppBundle\Entity\Attachment $attachment
//     *
//     * @return $this
//     */
//    public function addAttachment(\AppBundle\Entity\Attachment $attachment)
//    {
//        $this->attachments->add($attachment);
//
//        $attachment->setEmail($this);
//
//        // $this->attachments[] = $attachment;
//
//        return $this;
//    }
//
//    /**
//     * Remove attachment
//     *
//     * @param \AppBundle\Entity\Attachment $attachment
//     */
//    public function removeAttachment(\AppBundle\Entity\Attachment $attachment)
//    {
//        $this->attachments->removeElement($attachment);
//    }
//
//    /**
//     * Get attachments
//     *
//     * @return \Doctrine\Common\Collections\Collection
//     */
//    public function getAttachments()
//    {
//        return $this->attachments;
//    }

    /**
     * @param File $image
     */
    public function setThumbFile(File $image = null)
    {
        $this->thumbFile = $image;

        if ($image instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return File
     */
    public function getThumbFile()
    {
        return $this->thumbFile;
    }

    /**
     * @param string $image
     */
    public function setThumb($image)
    {
        $this->thumb = $image;
    }

    /**
     * @return string
     */
    public function getThumb()
    {
        return $this->thumb;
    }

    /**
     * @param File $image
     */
    public function setSliderFile(File $image = null)
    {
        $this->sliderFile = $image;

        if ($image instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return File
     */
    public function getSliderFile()
    {
        return $this->sliderFile;
    }

    /**
     * @param string $image
     */
    public function setSlider($image)
    {
        $this->slider = $image;
    }

    /**
     * @return string
     */
    public function getSlider()
    {
        return $this->slider;
    }

    /**
     * @param File $image
     */
    public function setSloganBgFile(File $image = null)
    {
        $this->sloganBgFile = $image;

        if ($image instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return File
     */
    public function getSloganBgFile()
    {
        return $this->sloganBgFile;
    }

    /**
     * @param string $image
     */
    public function setSloganBg($image)
    {
        $this->sloganBg = $image;
    }

    /**
     * @return string
     */
    public function getSloganBg()
    {
        return $this->sloganBg;
    }

    /**
     * Set slogan
     *
     * @param string $slogan
     *
     * @return $this
     */
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;

        return $this;
    }

    /**
     * Get slogan
     *
     * @return string
     */
    public function getSlogan()
    {
        return $this->slogan;
    }


    /**
     * @param string $image
     */
    public function setApproachImage($image)
    {
        $this->approachImage = $image;
    }

    /**
     * @return string
     */
    public function getApproachImage()
    {
        return $this->approachImage;
    }

    /**
     * @param File $image
     */
    public function setApproachFile(File $image = null)
    {
        $this->approachFile = $image;

        if ($image instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return File
     */
    public function getApproachFile()
    {
        return $this->approachFile;
    }

    /**
     * Set approachCaption
     *
     * @param string $approachCaption
     *
     * @return $this
     */
    public function setApproachCaption($approachCaption)
    {
        $this->approachCaption = $approachCaption;

        return $this;
    }

    /**
     * Get approachCaption
     *
     * @return string
     */
    public function getApproachCaption()
    {
        return $this->approachCaption;
    }


    /**
     * @param string $image
     */
    public function setApproachImage1($image)
    {
        $this->approachImage1 = $image;
    }

    /**
     * @return string
     */
    public function getApproachImage1()
    {
        return $this->approachImage1;
    }

    /**
     * @param File $image
     */
    public function setApproachFile1(File $image = null)
    {
        $this->approachFile1 = $image;

        if ($image instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return File
     */
    public function getApproachFile1()
    {
        return $this->approachFile1;
    }

    /**
     * Set approachCaption
     *
     * @param string $approachCaption1
     *
     * @return $this
     */
    public function setApproachCaption1($approachCaption1)
    {
        $this->approachCaption1 = $approachCaption1;

        return $this;
    }

    /**
     * Get approachCaption1
     *
     * @return string
     */
    public function getApproachCaption1()
    {
        return $this->approachCaption1;
    }

    /**
     * @param string $image
     */
    public function setApproachImage2($image)
    {
        $this->approachImage2 = $image;
    }

    /**
     * @return string
     */
    public function getApproachImage2()
    {
        return $this->approachImage2;
    }

    /**
     * @param File $image
     */
    public function setApproachFile2(File $image = null)
    {
        $this->approachFile2 = $image;

        if ($image instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return File
     */
    public function getApproachFile2()
    {
        return $this->approachFile2;
    }

    /**
     * Set approachCaption
     *
     * @param string $approachCaption2
     *
     * @return $this
     */
    public function setApproachCaption2($approachCaption2)
    {
        $this->approachCaption2 = $approachCaption2;

        return $this;
    }

    /**
     * Get approachCaption2
     *
     * @return string
     */
    public function getApproachCaption2()
    {
        return $this->approachCaption2;
    }

    // Result image

    /**
     * @param string $image
     */
    public function setResultImage($image)
    {
        $this->resultImage = $image;
    }

    /**
     * @return string
     */
    public function getResultImage()
    {
        return $this->resultImage;
    }


    /**
     * @param File $image
     */
    public function setResultFile(File $image = null)
    {
        $this->resultFile = $image;

        if ($image instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return File
     */
    public function getResultFile()
    {
        return $this->resultFile;
    }

    /**
     * Set resultCaption
     *
     * @param string $resultCaption
     *
     * @return $this
     */
    public function setResultCaption($resultCaption)
    {
        $this->resultCaption = $resultCaption;

        return $this;
    }

    /**
     * Get resultCaption
     *
     * @return string
     */
    public function getResultCaption()
    {
        return $this->resultCaption;
    }

  /**
     * @param string $image
     */
    public function setResultImage1($image)
    {
        $this->resultImage1 = $image;
    }

    /**
     * @return string
     */
    public function getResultImage1()
    {
        return $this->resultImage1;
    }


    /**
     * @param File $image
     */
    public function setResultFile1(File $image = null)
    {
        $this->resultFile1 = $image;

        if ($image instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return File
     */
    public function getResultFile1()
    {
        return $this->resultFile1;
    }

    /**
     * Set resultCaption1
     *
     * @param string $resultCaption
     *
     * @return $this
     */
    public function setResultCaption1($resultCaption)
    {
        $this->resultCaption1 = $resultCaption;

        return $this;
    }

    /**
     * Get resultCaption
     *
     * @return string
     */
    public function getResultCaption1()
    {
        return $this->resultCaption1;
    }

    /**
     * @param string $image
     */
    public function setResultImage2($image)
    {
        $this->resultImage2 = $image;
    }

    /**
     * @return string
     */
    public function getResultImage2()
    {
        return $this->resultImage2;
    }


    /**
     * @param File $image
     */
    public function setResultFile2(File $image = null)
    {
        $this->resultFile2 = $image;

        if ($image instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return File
     */
    public function getResultFile2()
    {
        return $this->resultFile2;
    }

    /**
     * Set resultCaption2
     *
     * @param string $resultCaption
     *
     * @return $this
     */
    public function setResultCaption2($resultCaption)
    {
        $this->resultCaption2 = $resultCaption;

        return $this;
    }

    /**
     * Get resultCaption
     *
     * @return string
     */
    public function getResultCaption2()
    {
        return $this->resultCaption2;
    }


}

