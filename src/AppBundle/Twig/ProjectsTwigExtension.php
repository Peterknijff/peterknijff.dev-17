<?php

namespace AppBundle\Twig;

use Doctrine\ORM\EntityManager;

class ProjectsTwigExtension extends \Twig_Extension
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('getProjects', [$this, 'getProjects'])
        ];
    }

    /**
     * @param $amount
     * @return array
     */
    public function getProjects($amount = 5)
    {
        $repository = $this->em->getRepository('AppBundle:Project');
        $projects = $repository->findBy(['enabled' => 1] , ['datetime' => 'DESC'], $amount);
        return $projects;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'app.footer_projects';
    }
}