<?php

namespace AppBundle\Twig;

use Doctrine\ORM\EntityManager;

class ExperienceTwigExtension extends \Twig_Extension
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('getExperiences', [$this, 'getExperiences'])
        ];
    }

    /**
     * @return array
     */
    public function getExperiences()
    {
        $repository = $this->em->getRepository('AppBundle:Experience');
        $experiences = $repository->findBy(['enabled' => 1] , ['dateEnd' => 'DESC']);
        return sizeof($experiences);
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'app.experiences';
    }
}