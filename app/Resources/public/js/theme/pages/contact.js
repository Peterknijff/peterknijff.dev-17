$(document).ready(function() {
    
    /* ======= jQuery form validator ======= */ 
    /* Ref: http://jqueryvalidation.org/documentation/ */   
    $("#contact-form").validate({
		messages: {
		
		    name: {
    			required: 'Please enter your name' //You can customise this message
			},
			email: {
				required: 'Please enter your email' //You can customise this message
			},			
			message: {
				required: 'Please enter your message' //You can customise this message
			}
			
		}
		
	});

    var map;
    jQuery(document).ready(function(){

        map = new GMaps({
            div: '#map',
            lat: 51.912985,
            lng: 4.448158
        });
        map.addMarker({
            lat: 51.912985,
            lng: 4.448158,
            title: 'Rotterdam',
            infoWindow: {
                content: '<h5 class="title">Headquarters</h5><p><span class="region">Hooidrift 155B2</span><br><span class="postal-code">3023KM</span><br><span class="country-name">Nederland</span></p>'
            }

        });

    });

});



