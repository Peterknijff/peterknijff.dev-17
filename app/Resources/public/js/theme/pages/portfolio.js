$(document).ready(function() {

    /* ======= Blog page masonry ======= */
    /* Ref: http://desandro.github.io/masonry/index.html */

    jQuery(window).on('load', function() {
        var $container = $('#portfolio-mansonry');
        $container.imagesLoaded(function () {
            $container.masonry({
                itemSelector: '.post'
            });
        });
    });

    $('.filter').customSelect();

    var $grid = $('.project-list').isotope({
        // options
        itemSelector: '.project-item',
        layoutMode: 'fitRows'
    });

    // filter functions
    var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt( number, 10 ) > 50;
        },
        // show if name ends with -ium
        ium: function() {
            var name = $(this).find('.name').text();
            return name.match( /ium$/ );
        }
    };

    // bind filter button click
    var $filterButtonGroup = $('.filter');

    function getHashFilter() {
        // get filter=filterName
        var matches = location.hash.match( /filter=([^&]+)/i );
        var hashFilter = matches && matches[1];
        return hashFilter && decodeURIComponent( hashFilter );
    }

    $filterButtonGroup.on( 'change', function() {
        var filterAttr = $( this ).attr('value');
        // set filter in hash
        if(getHashFilter() !==  filterAttr ){
            location.hash = 'filter=' + encodeURIComponent( filterAttr );
        }
    });

    var isIsotopeInit = false;

    function onHashchange() {
        var hashFilter = getHashFilter();
        if ( !hashFilter && isIsotopeInit ) {
            return;
        }

        isIsotopeInit = true;

        // filter isotope
        $grid.isotope({
            itemSelector: '.project-item',
            layoutMode: 'fitRows',
            // use filterFns
            filter: filterFns[ hashFilter ] || hashFilter
        });

        // set selected class on button
        if ( hashFilter ) {
            $filterButtonGroup.find('[selected=selected]').attr('selected', '');
            $filterButtonGroup.find('[value="' + hashFilter + '"]').attr('selected', 'selected');
            $filterButtonGroup.find('[value="' + hashFilter + '"]').trigger('change');
        }
    }

    $(window).on( 'hashchange', onHashchange );

    // trigger event handler to init Isotope
    onHashchange();
});