$(document).ready(function() {
    $('.blog-slider').flexslider({
        animation: "fade",
        pauseOnHover: true,
        directionNav: true, //remove the default direction-nav - https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties
        controlNav: false, //remove the default control-nav
        slideshowSpeed: 8000
    });

    $('.compact-slider').flexslider({
        animation: "fade",
        pauseOnHover: true,
        directionNav: true, //remove the default direction-nav - https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties
        controlNav: false, //remove the default control-nav
        slideshowSpeed: 8000
    });
});